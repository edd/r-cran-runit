This is the Debian GNU/Linux r-cran-runit package of RUnit, a package
that implements a standard Unit Testing framework for R. RUnit was written
by Matthias Burger, Klaus Juenemann and Thomas Koenig.

This package was created by Dirk Eddelbuettel <edd@debian.org>. The
sources were downloaded from CRAN at 
	http://cran.r-project.org/src/contrib/

The package was renamed from its upstream name 'RUnit' to
'r-cran-runit' to fit the pattern of CRAN (and non-CRAN) packages for
R, and to avoid clashing with the existing remote logging tool runit.

Copyright (C) 2004 - 2008 Matthias Burger, Klaus Juenemann and Thomas Koenig 

License: GPL (v2)

On a Debian GNU/Linux system, the GPL license (version 2) is included
in the file /usr/share/common-licenses/GPL-2.

For reference, the upstream DESCRIPTION [with lines broken to 80 cols,
and indented] file is included below:

  Package:      RUnit
  Version:      0.4.17
  Date:         2007/05/21
  Title:        R Unit test framework
  Author:       Matthias Burger <burgerm@users.sourceforge.net>, 
                Klaus Juenemann <k.junemann@gmx.net>, 
                Thomas Koenig <thomas.koenig@epigenomics.com> 
  Maintainer:   Matthias Burger <burgerm@users.sourceforge.net>
  LazyLoad:     yes
  Depends:      R (>= 1.9.0), utils (>= 1.9.0), methods (>= 1.9.0)
  Description:  R functions implementing a standard Unit Testing framework, 
                with additional code inspection and report generation tools 
  License:      GPL 2
  URL:          https://sourceforge.net/projects/runit/
  Packaged: Mon May 21 13:43:01 2007; burger
